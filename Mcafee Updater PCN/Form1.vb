﻿Imports Microsoft.Win32

Public Class Form1

    Public exe As String
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ContextMenuStrip3.Visible = False
        readdatdate()
        readdatversion()
        getdirectory()

    End Sub
    Dim i As Integer = 0
    Public Sub getdirectory()
        Dim path As String = Application.StartupPath & "\updates"
        Dim folderinfo As New IO.DirectoryInfo(path)
        Dim arrfilesinfolder() As IO.FileInfo
        Dim fileinfolder As IO.FileInfo
        arrfilesinfolder = folderinfo.GetFiles()
        For Each fileinfolder In arrfilesinfolder
            Dim bytes As String = fileinfolder.Length
            Dim mb As String
            mb = (bytes / 1024) / 1024
            DataGridView1.Rows.Add(fileinfolder.Name, Format(CByte(mb), "#0.00") & " MB", fileinfolder.Extension)
        Next
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        DataGridView1.Rows.Clear()
        getdirectory()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Button1.Click

        Dim value As Object = DataGridView1.CurrentCell.Value.ToString
        If IsDBNull(value) Then
            MsgBox("No updates found!", MsgBoxStyle.Information, "Error")
        Else
            'MsgBox(CType(value, String))
            'TextBox1.Text = CType(value, String)
            executeselecteditem(CType(value, String))
        End If
    End Sub

    Public Sub readdatdate()
        Dim readValue = My.Computer.Registry.GetValue(
    "HKEY_LOCAL_MACHINE\Software\Wow6432Node\McAfee\AVEngine", "AVDatDate", Nothing)
        If readValue = Nothing Then
            ToolStripStatusLabel2.Text = "Not Found!"
        Else
            ToolStripStatusLabel2.Text = readValue.ToString
        End If

        'MsgBox("The value is " & readValue)
    End Sub

    Public Sub readdatversion()
        Dim readValue2 = My.Computer.Registry.GetValue(
    "HKEY_LOCAL_MACHINE\Software\Wow6432Node\McAfee\AVEngine", "AVDatVersion", Nothing)
        If readValue2 = Nothing Then
            ToolStripStatusLabel2.Text = "Not Found!"
        Else
            ToolStripStatusLabel4.Text = readValue2.ToString
        End If

        'MsgBox("The value is " & readValue)
    End Sub

    Public Sub executeselecteditem(program As String)
        Dim startfile As String = Application.StartupPath & "\updates\" & program
        'Shell(Application.StartupPath & "\updates\" & ListBox1.SelectedItem.ToString, AppWinStyle.NormalFocus, True, 10)
        'MsgBox(startfile)
        Try
            Dim p As New System.Diagnostics.Process
            Dim s As New System.Diagnostics.ProcessStartInfo(startfile)
            s.UseShellExecute = True
            s.WindowStyle = ProcessWindowStyle.Normal
            p.StartInfo = s
            p.Start()
        Catch ex As Exception
            MessageBox.Show("File " & startfile & " couldnt be found!", "www.interloper.nl", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        End
    End Sub

    Public Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentDoubleClick
        Dim value As Object = DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
        If IsDBNull(value) Then
            '
        Else
            ' MsgBox(CType(value, String))
            'TextBox1.Text = CType(value, String)
            executeselecteditem(CType(value, String))
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick_1(sender As Object, e As MouseEventArgs) Handles DataGridView1.MouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then

            ContextMenuStrip3.Visible = True
            ContextMenuStrip3.Location = New Point(e.Y, e.X)
            Label1.Text = "x: " & e.X & "y: " & e.Y
            Label2.Text = "x: " & ContextMenuStrip3.Location.X & "y: " & ContextMenuStrip3.Location.Y
        End If

    End Sub
End Class
